package com.handy.kafkaprovider.service;

import com.handy.commonkafka.KafkaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author hs
 * @Description: {}
 * @date 2019/12/20 11:37
 */
@Service
public class ProviderService {

    @Autowired
    private KafkaUtil kafkaUtil;

    /**
     * 发送消息到kafka
     *
     * @param str
     * @return
     */
    public String send(String str,String key) {
        kafkaUtil.dropTemplate("test_topic", key,"提供者推送消息:" + str);
        return "提交成功";
    }

    public String send(String str) {
        kafkaUtil.dropTemplate("test_topic", "提供者推送消息:" + str);
        return "提交成功";
    }

}
