package com.handy.kafkaprovider.controller;

import com.handy.kafkaprovider.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author hs
 * @Description: {}
 * @date 2019/12/20 11:36
 */
@RestController
public class ProviderController {

    @Autowired
    private ProviderService providerService;

    /**
     * 发送消息
     * @param str
     * @return
     */
    @RequestMapping("/test")
    public String sendOne(String str,String key) {
        return providerService.send(str,key);
    }

    /**
     * 发送消息
     * @param str
     * @return
     */
    @RequestMapping("/test1")
    public String sendOne(String str) {
        return providerService.send(str);
    }

}
