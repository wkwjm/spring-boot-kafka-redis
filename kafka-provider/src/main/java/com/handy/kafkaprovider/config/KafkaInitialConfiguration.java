package com.handy.kafkaprovider.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author hs
 * @Description: {}
 * @date 2020/1/12 14:48
 */
@Configuration
public class KafkaInitialConfiguration {

    /**
     * 创建TopicName为test_topic的Topic并设置分区数为8以及副本数为1
     * 注意:分区只能增加不能减少
     * 分区对应的数量为kafka集群数量
     *
     * @return
     */
    @Bean
    public NewTopic initialTopic() {
        return new NewTopic("test_topic", 3, (short) 1);
    }
}
