package com.handy.kafkaconsumer.util;

import com.handy.commonredis.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hs
 * @Description: {}
 * @date 2019/12/30 15:15
 */
@Component
public class IdGenerator {

    private RedisUtil redisUtil;

    @Autowired
    public IdGenerator(RedisUtil redisUtil) {
        this.redisUtil = redisUtil;
        String key = "test_id";
        if (!this.redisUtil.exists(key)) {
            this.redisUtil.setIncr(key, 0);
        }
    }

    /**
     * 从redis获取到唯一oid
     *
     * @return
     */
    public long GenId() {
        return this.redisUtil.getIncr("test_id");
    }

}
