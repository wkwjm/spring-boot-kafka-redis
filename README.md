# spring boot-kafka-redis

#### 介绍
spring boot集成kafka,redis

#### 软件架构
软件架构说明


#### 安装教程

1.  docker-compose
#####
     version: '2.1'
     services:
       zoo1:
         image: zookeeper:3.4.9
         hostname: 49.235.186.43
         ports:
           - "2181:2181"
         environment:
           ZOO_MY_ID: 1
           ZOO_PORT: 2181
           ZOO_SERVERS: server.1=zoo1:2888:3888
         volumes:
           - ./zk-single-kafka-single/zoo1/data:/data
           - ./zk-single-kafka-single/zoo1/datalog:/datalog
     
       kafka1:
         image: confluentinc/cp-kafka:5.3.1
         hostname: kafka1
         ports:
           - "9092:9092"
         environment:
           KAFKA_ADVERTISED_LISTENERS: LISTENER_DOCKER_INTERNAL://kafka1:19092,LISTENER_DOCKER_EXTERNAL://49.235.186.43:9092
           KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: LISTENER_DOCKER_INTERNAL:PLAINTEXT,LISTENER_DOCKER_EXTERNAL:PLAINTEXT
           KAFKA_INTER_BROKER_LISTENER_NAME: LISTENER_DOCKER_INTERNAL
           KAFKA_ZOOKEEPER_CONNECT: "zoo1:2181"
           KAFKA_BROKER_ID: 1
           KAFKA_LOG4J_LOGGERS: "kafka.controller=INFO,kafka.producer.async.DefaultEventHandler=INFO,state.change.logger=INFO"
           KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
         volumes:
           - ./zk-single-kafka-single/kafka1/data:/var/lib/kafka/data
         depends_on:
           - zoo1

2.  redis
#####
    version: '3.1'
    services:
      master:
        image: redis
        container_name: redis-master
        ports:
          - 6379:6379

