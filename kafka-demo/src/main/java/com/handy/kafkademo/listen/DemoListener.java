package com.handy.kafkademo.listen;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;


/**
 * @author hs
 * @Description: {}
 * @date 2019/12/20 10:45
 */
@Component
public class DemoListener {

    /**
     * 声明consumerID为demo，监听topicName为topic.quick.demo的Topic
     *
     * @param msgData
     */
    @KafkaListener(id = "demo", topics = "topic.quick.demo")
    public void listen(String msgData) {
        System.out.println("demo receive : " + msgData);
    }

}
