package com.handy.commonkafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author hs
 * @Description: {}
 * @date 2019/12/20 11:21
 */
@Repository
public class KafkaUtil {

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    public void dropTemplate(String topic, String key, Object template) {
        kafkaTemplate.send(
                topic,
                key,
                template
        );
    }

    public void dropTemplate(String topic, Object template) {
        kafkaTemplate.send(
                topic,
                template
        );
    }

}
